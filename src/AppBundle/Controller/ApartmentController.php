<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Apartment;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;

class ApartmentController extends FOSRestController
{

    /**
     * @Rest\Get("/api/apartment")
     */
    public function getAllAction()
    {
        //TODO implement some kind of XXS prevention either on save and/or on display
        $arrayOfApartments = $this->getDoctrine()->getRepository('AppBundle:Apartment')->findAll();

        $view = new View();
        $view->setFormat('json');
        $view->setData($arrayOfApartments);
        return $this->handleView($view);
    }

    /**
     * @Rest\Get("/api/apartment/{id}")
     * @param $id
     * @return Response
     */
    public function findByIdAction($id)
    {
        //TODO implement some kind of XXS prevention either on save and/or on display
        $view = new View();
        $view->setFormat('json');
        $apartment = $this->getApartmentById($id);
        $view->setData($apartment);

        return $this->handleView($view);
    }

    /**
     * @Rest\Post("/api/apartment")
     * @param Request $request
     * @return Response
     */
    public function postAction(Request $request)
    {
        $newApartment = new Apartment();
        $newApartment->setMoveInDate(new \DateTime($request->get('moveInDate')));
        $newApartment->setStreet($request->get('street'));
        $newApartment->setPostCode($request->get('postCode'));
        $newApartment->setCity($request->get('city'));
        $newApartment->setCountryCode($request->get('countryCode'));
        $newApartment->setContactMailAddress($request->get('contactMailAddress'));

        $errors = $this->validateApartment($newApartment);

        $view = new View();
        $view->setFormat('json');

        if (count($errors)) {
            $view->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
            $view->setData(array("success" => false, "errors" => $errors));
        } else {
            //save the new apartment
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($newApartment);
            $doctrine->flush();

            //Generate a token for the apartment and save it for later use
            $tokenGenerator = $this->get('apartment_token_generator');
            $apartmentToken = $tokenGenerator->generateToken($newApartment->getId());
            $doctrine->persist($apartmentToken);
            $doctrine->flush();

            //send confirmation mail with token to the contact mail address
            $confirmationMailer = $this->get('confirmation_mailer');
            $confirmationMailer->sendApartmentCreatedConfirmation($newApartment, $apartmentToken);

            $view->setStatusCode(Response::HTTP_OK);
            $view->setData(array("success" => true));
        }
        return $this->handleView($view);
    }

    /**
     * @Rest\Put("/api/apartment/{id}/{token}")
     *
     * @param Request $request
     * @param int $id
     * @param string $token
     * @return Response
     */

    public function putAction(Request $request, int $id, string $token)
    {
        //check if token is valid for given apartment id
        $apartmentToken = $this->getDoctrine()
            ->getRepository('AppBundle:ApartmentToken')
            ->findOneBy(array('apartmentId' => $id, "token" => $token));
        if ($apartmentToken === null) {
            throw $this->createNotFoundException("The given Token is invalid");
        }

        $updatedApartment = new Apartment();
        $updatedApartment->setId($id);
        $updatedApartment->setMoveInDate(new \DateTime($request->get('moveInDate')));
        $updatedApartment->setStreet($request->get('street'));
        $updatedApartment->setPostCode($request->get('postCode'));
        $updatedApartment->setCity($request->get('city'));
        $updatedApartment->setCountryCode($request->get('countryCode'));
        $updatedApartment->setContactMailAddress($request->get('contactMailAddress'));

        $view = new View();
        $view->setFormat('json');

        //validate apartment
        $errors = $this->validateApartment($updatedApartment);
        if (count($errors)) {
            $view->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
            $view->setData(array("success" => false, "errors" => $errors));
        } else {
            $doctrine = $this->getDoctrine()->getManager();
            var_dump($updatedApartment);
            $doctrine->merge($updatedApartment);
            $doctrine->flush();

            $view->setStatusCode(Response::HTTP_OK);
            $view->setData(array("success" => true));
        }
        return $this->handleView($view);
    }

    /**
     * @param int $id
     * @return Apartment|null
     */
    private function getApartmentById(int $id): ?Apartment
    {
        /** @var Apartment|null $apartment */
        $apartment = $this->getDoctrine()->getRepository('AppBundle:Apartment')->find($id);
        if ($apartment === null) {
            throw $this->createNotFoundException("The given ApartmentId could'nt be found");
        }
        return $apartment;
    }

    /**
     * @param Apartment $apartment
     * @return array
     */
    private function validateApartment(Apartment $apartment)
    {
        $validator = $this->get("validator");
        $violations = $validator->validate($apartment);
        $errors = [];

        if (count($violations) > 0) {
            foreach ($violations as $violation) {
                /** @var ConstraintViolation $violation */
                $errors['errors'][] = ['field' => $violation->getPropertyPath(), 'message' => $violation->getMessage()];
            }
        }
        return $errors;
    }


}
