<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * Class SecurityToken
 * @package AppBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="apartmentToken")
 */
class ApartmentToken
{

    public function __construct(int $apartmentId, string $token)
    {
        $this->apartmentId = $apartmentId;
        $this->token = $token;
    }

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     */
    private $apartmentId;

    /**
     * @ORM\Column(type="string")
     */
    private $token;

    /**
     * @return mixed
     */
    public function getApartmentId()
    {
        return $this->apartmentId;
    }

    /**
     * @param mixed $apartmentId
     * @return ApartmentToken
     */
    public function setApartmentId($apartmentId)
    {
        $this->apartmentId = $apartmentId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     * @return ApartmentToken
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

}