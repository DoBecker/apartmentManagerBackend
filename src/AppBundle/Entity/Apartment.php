<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Apartment
 * @package AppBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="apartment")
 */
class Apartment
{
    /**
     * @Assert\Type("integer")
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Assert\Date()
     * @Assert\NotBlank()
     * @ORM\Column(type="date")
     */
    private $moveInDate;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string")
     */
    private $street;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string")
     */
    private $postCode;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string")
     */
    private $city;

    /**
     * @Assert\NotBlank()
     * @Assert\Country()
     * @ORM\Column(type="string", length=2)
     */
    private $countryCode;

    /**
     * @Assert\NotBlank()
     * @Assert\Email()
     * @ORM\Column(type="string")
     */
    private $contactMailAddress;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Apartment
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMoveInDate()
    {
        return $this->moveInDate;
    }

    /**
     * @param mixed $moveInDate
     * @return Apartment
     */
    public function setMoveInDate($moveInDate)
    {
        $this->moveInDate = $moveInDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param mixed $street
     * @return Apartment
     */
    public function setStreet($street)
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
     * @param mixed $postCode
     * @return Apartment
     */
    public function setPostCode($postCode)
    {
        $this->postCode = $postCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return Apartment
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * @param mixed $countryCode
     * @return Apartment
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContactMailAddress()
    {
        return $this->contactMailAddress;
    }

    /**
     * @param mixed $contactMailAddress
     * @return Apartment
     */
    public function setContactMailAddress($contactMailAddress)
    {
        $this->contactMailAddress = $contactMailAddress;
        return $this;
    }

}