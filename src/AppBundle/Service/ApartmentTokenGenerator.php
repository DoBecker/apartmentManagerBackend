<?php

namespace AppBundle\Service;


use AppBundle\Entity\ApartmentToken;

class ApartmentTokenGenerator
{
    const TOKEN_LENGTH = 255;

    /**
     * @param int $apartmentId
     * @param int $length
     * @return ApartmentToken
     */
    public function generateToken(int $apartmentId, int $length = self::TOKEN_LENGTH)
    {
        $randomToken = bin2hex(random_bytes(floor($length / 2)));
        return new ApartmentToken($apartmentId, $randomToken);
    }
}