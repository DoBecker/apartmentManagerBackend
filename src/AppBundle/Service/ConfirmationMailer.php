<?php
/**
 * Created by PhpStorm.
 * User: Saturn
 * Date: 07.09.2017
 * Time: 23:50
 */

namespace AppBundle\Service;


use AppBundle\Entity\Apartment;
use AppBundle\Entity\ApartmentToken;
use Symfony\Component\Config\Definition\Exception\Exception;

class ConfirmationMailer
{
    const APARTMENT_CREATED_SUBJECT = "Neues Apartment im Apartment Manager";

    private $mailer;

    /**
     * ConfirmationMailer constructor.
     * @param \Swift_Mailer $mailer
     */
    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param Apartment $apartment
     * @param ApartmentToken $apartmentToken
     */
    public function sendApartmentCreatedConfirmation(Apartment $apartment, ApartmentToken $apartmentToken): void
    {
        $apartmentURL = "www.apartmentManager.com/apartmentEditor/{$apartment->getId()}/{$apartmentToken->getToken()}";
        $message = (new \Swift_Message(self::APARTMENT_CREATED_SUBJECT))
            ->setFrom("apartmentManager@apartmentManager.com")
            ->setTo($apartment->getContactMailAddress())
            ->setBody("Ihr  Apartment wurde angelegt!<br>Klicken Sie folgenden Link um es zu bearbeiten:<br>" . $apartmentURL, "text/html");

        $mailsSent = $this->mailer->send($message);
        if (!$mailsSent) {
            throw new Exception("Could't send Mail");
        }
    }
}