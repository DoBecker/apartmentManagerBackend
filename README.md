Apartment Manager Backend
========================

This is a backend REST API for apartment management

What do you need to get startet? 
--------------
**PHP 7.1^**
```shell
sudo apt-add-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install php7.1
sudo apt-get install php7.1-xml
sudo apt-get install php7.1-mysql
```
**mysql**
```shell
sudo apt-get install mysql-server
```
**git**
```shell
sudo apt-get install git
```
**composer**
```shell
sudo apt-get install composer
```
**clone the repo**
```shell
git clone https://gitlab.com/DoBecker/apartmentManagerBackend.git
```
**intall all dependencies**
```shell
cd apartmentManagerBackend
composer update

Database Params:
Enter **your** Database informations
SMTP Params:
transport: gmail
host: null
user: apartmentmanagertest@googlemail.com
password: Test12345
```
**create the Databse**
```shell
php bin/console doctrine:database:create
```
**create tables**
```shell
php bin/console doctrine:schema:update --force
```


**run the server and start !**
```shell
php bin/console server:run
```

What are the endpoints?
--------------

To get a list of all apartments:  
GET /api/apartment  
To get a specific apartment:  
GET /api/apartment/{id}  
To create a new apartment:  
POST /api/apartment  
To update an existing apartment  
PUT /api/apartment/{id}/{token}  
(The token ist provided in an email send to the contact mail address of the apartment on creation)

**Both the POST and the PUT endpoints expect the following request _non-empty_ paramters:**  
moveInDate  
street  
postCode  
city  
countryCode  
contactMailAddress  
